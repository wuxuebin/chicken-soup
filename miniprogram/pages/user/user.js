let updatetemplaceid = require('../../config/config.js')
const app = getApp()
Page({
  data: {
    PageCur: 'basics',
    openid:app.globalData.openid
  },
  NavChange(e) {
    this.setData({
      PageCur: e.currentTarget.dataset.cur
    })
  },
  onShareAppMessage() {
    return {
      title: '心灵鸡汤大全',
      path: '/pages/index/index'
    }
    
  },

  /**
   * 
   */
  toNone(){
   wx.showToast({
     title: '开发中。。',
     icon:"none"
   })
  },
  tocollection(e){
    let type =e.currentTarget.dataset.type
   wx.navigateTo({
     url:'../collection/collection?type='+type
   })
  },
  showQrcode(){
    wx.previewImage({
      current: 'cloud://test-rwbr1.7465-test-rwbr1-1302108029/wx/微信图片_20200714140417.png', 
      urls: ['cloud://test-rwbr1.7465-test-rwbr1-1302108029/wx/微信图片_20200714140417.png','cloud://test-rwbr1.7465-test-rwbr1-1302108029/wx/微信图片_20200513184013.jpg'] 
    })
  },
  showWechatCode(){
    wx.previewImage({
      current: 'cloud://test-rwbr1.7465-test-rwbr1-1302108029/wx/微信图片_20200513184013.jpg',
      urls: ['cloud://test-rwbr1.7465-test-rwbr1-1302108029/wx/微信图片_20200513184013.jpg', 'cloud://test-rwbr1.7465-test-rwbr1-1302108029/wx/微信图片_20200714140417.png']
    }) 
  },
  async showRelease(){

    wx.requestSubscribeMessage({
      tmplIds: [updatetemplaceid.updatetemplaceid],
      success(res) {
        console.log(res)
        if (res[updatetemplaceid.updatetemplaceid] == 'accept'){
          console.log('成功')
          // console.log('openid',openid)
          wx.cloud.callFunction({
            name:'subscribeMessage'
          }).then(res=>{
            console.log(res)
          })
        }else{
          console.log('失败') 
        }
       }
    })
  },
  About(){
   wx.navigateTo({
     url: '../about/about',
   })
  },
  toblog(){
    wx.navigateToMiniProgram({
      appId: 'wx4e77bb31b1bd3a7b',
      success(res) {
        // 打开成功
      }
    })
  },
  onLoad(){

    let data = wx.getStorageSync('openid')
    console.log(data)
    if (data.data){
      this.setData({
        openid : data.data
      })
    }else{
      this.setData({
        openid :app.globalData.openid
      })
      wx.setStorage({
        key:'openid',
        data:app.globalData.openid
      })
    }
    // wx.cloud.callFunction({
    //   name:'author'
    // }).then(res=>{
    //   console.log(res.result)
    //   this.setData({
    //     author: res.result
    //   })
    // })
  }
})