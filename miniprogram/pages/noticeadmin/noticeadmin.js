// pages/noticeadmin/noticeadmin.js
const api = require('../../api/api.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    searchval:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    let res = await api.getnotice()
    let notice =res.data[0] 
    console.log(notice)
    this.setData({
      searchval:notice.text,
      id:notice._id
    })
  },
  async search (){
    let that = this
    wx.showLoading({
      title: '更新中',
      icon:'none'
    })
    console.log(that.data.id,that.data.searchval)
    await api.setnotice(that.data.id,that.data.searchval)
     wx.hideLoading({
       success: (res) => {},
     })
     
  },
searchtap(e){
  console.log(e.detail.value)
   this.setData({
    searchval:e.detail.value
   })
},
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})