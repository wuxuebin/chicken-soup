// pages/collection/collection.js
const api = require('../../api/api.js')
let app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active: null,
    postRelated:[],
    page:1
  },
  // onReady(){
  //   wx.showShareMenu({
  //     withShareTicket: true,
  //     menus: ['shareAppMessage', 'shareTimeline']
  //   })
  // },
  onShareTimeline(){
    return {
      title:'hello',
      query:{
        test:1,
        type:2
      },
      imageUrl:this.data.list[0].imgurl
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
   console.log(options)
   let type = options.type;
    if (type == 1){
      this.getPostRelated(parseInt(type))
      this.setData({
        active:0
      })
    }else{
       this.getPostRelated(parseInt(type))
      this.setData({
        active:1
      })
    }
    // await this.getPostRelated(parseInt(type))
  },
    /**
   * 获取收藏或喜欢列表
   */
  getPostRelated: async function (type) {
    let that = this;
    let page = that.data.page;
    let where = {
      type: type,
      openId: app.globalData.openid
    };
    let postRelated = await api.getPostRelated(where, page)
    if (postRelated.data.length === 0) {
      that.setData({
        nomore: true,
        state:true
      })
      if (page === 1) {
        that.setData({
          nodata: true
        })
      }
    }
    else {
      that.setData({
        page: page + 1,
        postRelated: that.data.postRelated.concat(postRelated.data),
        state:true
      })
    }
  },
    /**
 * 点击文章明细
 */
bindPostDetail: async function (e) {
  console.log(e)
  let blogId = e.currentTarget.dataset._id;

  wx.navigateTo({
    url: '/pages/details/details?id=' + blogId
  })
},
async onChange (event) {
    let that = this;
    console.log(event);
    that.setData({
      postRelated: [],
      nomore: false,
      nodata: false,
      page: 1
    })
    await that.getPostRelated(event.detail.name+1)
  },


  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})