// pages/subscribe/subscribe.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  // 订阅消息
  showSubscribe(){
   wx.navigateTo({
     url: '../subadmin/subadmin',
   })
  },
  // 通告管理
  showAdvert(){
   wx.navigateTo({
     url: '../noticeadmin/noticeadmin',
   })
  },
  // 评论管理
  showComment(){
    wx.navigateTo({
      url: '../Commentadmin/Commentadmin',
    })
  },
  // 文章管理
  showArticle(){
    wx.navigateTo({
      url: '../postadmin/postadmin',
    })
  },
// 新增文章
showReleaseModal(){
    wx.navigateTo({
      url: '../admin/admin',
    })
},
// 分类管理
showClassify(){
  wx.navigateTo({
    url: '../tabadmin/tabadmin',
  })
},
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})