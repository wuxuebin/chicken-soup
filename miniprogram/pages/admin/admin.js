// pages/admin/admin.js
const api = require('../../api/api.js')
const utils = require('../../utils/utils.js')
Page({
  /**
   * 页面的初始数据
   */
  data: {
    typemsg:'每日一句',
    chinmsg:'',
    engmsg:'',
    switch1Checked: true,
    imgurl:''
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad:async function (options) {
    let tablist = await api.gettab()
    this.setData({
      radioItems:tablist.data
    })
  },
  chooseImage: function () {
    // var openid = wx.getStorageSync("openid");
    var that = this;
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        var tempFilePaths = res.tempFilePaths;
        console.log(tempFilePaths[0]);
        that.setData({
          imgurl: tempFilePaths[0]
        })
      }
    })
  },
  radioChange(e){
    console.log(e)
    this.setData({
      typemsg:e.detail.value
    })
  },
  async sen (){
    wx.showLoading({
      title: '添加中...',
    })
    let imgurl = ''
    if (this.data.typemsg && (this.data.chinmsg || this.data.engmsg) && this.data.imgurl){
      let data = +new Date
      let index = this.data.imgurl.lastIndexOf('.')
      let weiba = this.data.imgurl.slice(index)
      let that =this
      try {
        if (!this.state){
          let res = await wx.cloud.uploadFile({
            cloudPath: 'title/' + data + weiba,
            filePath: this.data.imgurl, // 文件路径
          })
          imgurl = res.fileID
        }

        let checktype = await api.msgSecCheck(this.data.typemsg)
        let checkchinmsg = await api.msgSecCheck(this.data.chinmsg)
        let checkengmsg = await api.msgSecCheck(this.data.engmsg)
        let checkimg = await api.imgSecCheck(this.data.imgurl)
        console.log(checktype.result, checkchinmsg.result, checkengmsg.result, checkimg.result)
        if (!(checktype.result && checkchinmsg.result && checkengmsg.result)){
          wx.showToast({
            title: '文本不合法',
            icon:'none'
          })
          return
        }
        if (!checkimg){
          wx.showToast({
            title: '图片不合法',
            icon: 'none'
          })
          return
        }
        console.log(this.state)
        let parameter = {
          typemsg: that.data.typemsg,
          chinmsg: that.data.chinmsg,
          engmsg: that.data.engmsg,
          imgurl: this.state ? this.data.imgurl : imgurl,
          isshow: that.data.switch1Checked?1:0,
          createTime: utils.dateFormat("YYYY-mm-dd",new Date),
          good: parseInt(Math.random() * (100 - 10 + 1) + 10, 10),
          collection:1,
          download:1
        }
        if (!this.state){
          this.setData({
            fileID: imgurl
          })
        }

        let msg = await api.adddata(parameter)
        console.log(msg)
        wx.showToast({
          title: '数据添加成功'
        })
        this.setData({
          chinmsg:'',
          engmsg:'',
          imgurl:''
        })
      } catch (err){
        console.log(err)
      }

    }else{
      wx.showToast({
        title: '内容为空',
        icon:'none'
      })
    }
  },
  picimg(e){
    this.state = true
    console.log(111)
    this.setData({
      imgurl:e.detail.value
    })
  },
  typetap(e){
    // console.log(e.detail.value)
    this.setData({
      typemsg:e.detail.value
    })
  },
  chinmtap(e) {
    // console.log(e.detail.value)
    this.setData({
      chinmsg: e.detail.value
    })
  },
  engmtap(e) {
    // console.log(e.detail.value)
    this.setData({
      engmsg: e.detail.value
    })
  },
  delimg(){
    console.log(11)
    this.setData({
      imgurl:''
    })
  },
  switch1Change(e){
    this.setData({
      isshow: e.detail.value
    })
  }
})