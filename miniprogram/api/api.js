const db = wx.cloud.database()
const _ = db.command
/**
 * 添加数据
 * typemsg 分类
 * chinmsg 中文
 * engmsg 英文
 * imgurl 图片地址
 * isshow 是否显示
 * createTime 创建时间
 */
function adddata (data){
  return db.collection('ChickenSoup').add({
    data:{
      typemsg: data.typemsg,
      chinmsg: data.chinmsg,
      engmsg: data.engmsg,
      imgurl: data.imgurl,
      isshow: data.isshow,
      createTime: data.createTime,
      good:data.good
    }
  })
}
/**
 * 获取首页数据
 */
function getdata(pagenum = 0,type='每日一句'){
  return db.collection('ChickenSoup').where({
    isshow:1,
    typemsg: type
  }).orderBy('createTime', 'desc').limit(10).skip(pagenum*10).get()
}
/**
 * 获取全部数据包括隐藏的
 * @param {*} pagenum 
 * @param {*} type 
 */
function getalldata(pagenum = 0,type='每日一句'){
  return db.collection('ChickenSoup').where({
    typemsg: type
  }).orderBy('createTime', 'desc').limit(10).skip(pagenum*10).get()
}
/**
 * 搜索
 */
async function  search(val)  {
  let chinmsgsearch = await db.collection('ChickenSoup').where({
    chinmsg: db.RegExp({
      regexp: val,
      options: 'i',
    })
  }).orderBy('createTime', 'desc').get()
  console.log(chinmsgsearch)
  chinmsgsearch
  if (!chinmsgsearch.data.length){
   return db.collection('ChickenSoup').where({
       engmsg: db.RegExp({
       regexp: val,
       options: 'i',
     })
   }).orderBy('createTime', 'desc').get()
  }else{
    return chinmsgsearch
  }
}
/**
 * 检查文本安全
 */
function msgSecCheck(content) {
  console.log(content)
  return  wx.cloud.callFunction({
    name:'postsService',
    data:{
      content,
      action: 'checkPostComment'
    }
  })
}
/**
 * 检查图片安全
 */
function imgSecCheck(value) {
  return wx.cloud.callFunction({
    name: 'postsService',
    data: {
      value,
      action: 'checkimg'
    }
  })
}
/**
 * 获取轮播图数据
 */
function getswiper (){
  return db.collection('swiperList').where({
    id: _.gte(0)
  }).get()
}
/**
 * 点赞
 */
function like (openid,_id) {
  return db.collection('like').where({
    
  })
}
/**
 * 获取评论列表
 * @param {} page 
 * @param {*} postId 
 */
function getPostComments(page, postId) {
  return db.collection('mini_comments')
      .where({
          postId: postId,
          flag: 0
      })
      .orderBy('timestamp', 'desc')
      .skip((page - 1) * 10)
      .limit(10)
      .get()
}
/**
 * tab
 */
function gettab (){
  return db.collection('tablist').where({
    title:_.exists(true),
    type:_.exists(true)
  }).get()
}
/**
 * 获取详情数据
 */
function getdetails(_id){
  return db.collection('ChickenSoup').doc(_id).get()
}
/**
 * 
 * 获取通告
 */
function getnotice (){
  return db.collection('notice').where({
    text:_.exists(true)
  }).get()
}
/**
 * 新增评论
 */
function addPostComment(commentContent, accept) {
  return wx.cloud.callFunction({
      name: 'postsService',
      data: {
          action: "addPostComment",
          commentContent: commentContent,
          accept: accept
      }
  })
}
/**
 * 取消喜欢或收藏
 */
function deletePostCollectionOrZan(postId, type) {
  return wx.cloud.callFunction({
      name: 'postsService',
      data: {
          action: "deletePostCollectionOrZan",
          postId: postId,
          type: type
      }
  })
}

/**
 * 新增子评论
 * @param {} id 
 * @param {*} comments 
 */
function addPostChildComment(id, postId, comments, accept) {
  return wx.cloud.callFunction({
      name: 'postsService',
      data: {
          action: "addPostChildComment",
          id: id,
          comments: comments,
          postId: postId,
          accept: accept
      }
  })
}

/**
 * 新增用户点赞
 * @param {} data 
 */
function addPostZan(data) {
  let {chinmsg,createTime,engmsg,isshow,type,typemsg,imgurl} = data
  return wx.cloud.callFunction({
      name: 'postsService',
      data: {
        action:'addPostZan',
        postId:data._id,
        openId:data._openid,
        chinmsg,
        createTime,
        engmsg,
        isshow,
        type,
        typemsg,
        imgurl
      }
  })
}
/**
 * 获取海报的文章二维码url
 * @param {*} id 
 */
function getReportQrCodeUrl(id) {
  return wx.cloud.getTempFileURL({
      fileList: [{
          fileID: id,
          maxAge: 60 * 60, // one hour
      }]
  })
}

/**
 * 新增用户收藏文章
 */
function addPostCollection(data) {
  let {chinmsg,createTime,engmsg,isshow,type,typemsg,imgurl} = data
  return wx.cloud.callFunction({
      name: 'postsService',
      data: {
        action:'addPostCollection',
        postId:data._id,
        openId:data._openid,
        chinmsg,
        createTime,
        engmsg,
        isshow,
        type,
        typemsg,
        imgurl
      }
  })
}

/**
 * 获取收藏、点赞列表
 * @param {} page 
 */
function getPostRelated(where, page) {
  return db.collection('mini_posts_related')
      .where(where)
      .orderBy('createTime', 'desc')
      .skip((page - 1) * 10)
      .limit(10)
      .get()
}

/**
 * 新增文章二维码并返回临时url
 * @param {*} id 
 * @param {*} postId 
 * @param {*} comments 
 */
function addPostQrCode(postId, timestamp) {
  return wx.cloud.callFunction({
      name: 'postsService',
      data: {
          action: "addPostQrCode",
          timestamp: timestamp,
          postId: postId
      }
  })
}
/**
 * 查询可用的formId数量
 */
function querySubscribeCount(templateId) {
  return wx.cloud.callFunction({
      name: 'messageService',
      data: {
          action: "querySubscribeCount",
          templateId: templateId
      }
  })
}
/**
 * 
 * @param {*} templateIds 
 * 新增订阅消息
 */
function addSubscribeCount(templateIds) {
  return wx.cloud.callFunction({
      name: 'messageService',
      data: {
          action: "addSubscribeCount",
          templateIds: templateIds
      }
  })
}
/**
 * 获取评论列表
 */
function getCommentsList(page, flag) {
  return db.collection('mini_comments')
      .where({
          flag: flag
      })
      .orderBy('timestamp', 'desc')
      .skip((page - 1) * 10)
      .limit(10)
      .get()
}

/**
 * 更新评论状态
 * @param {*} id 
 * @param {*} flag 
 */

async function changeCommentFlagById(id, flag, postId, count) {
  console.log('flag',flag)
  console.log('id',id)
  try {
    let task1 = db.collection('mini_comments').doc(id).update({
      data: {
        flag: flag
      }
    })
    await task1
    return true;
  } catch (e) {
    console.error(e)
    return false;
  }
}
/**
 * 更新通告
 */

async function setnotice(id,text){
  try {
    return await db.collection('notice').doc(id).update({
      data:{
        text:text
      }
    })
  } catch (error) {
    return error
  }

 }
/**
 * 
 * @param {*} id 
 * @param {*} isshow 
 */
 async function updatepost(id,isshow){
  try {
    return await db.collection('ChickenSoup').doc(id).update({
      data:{
        isshow:isshow
      }
    })
  } catch (error) {
    return error
  }
 }
/**
 * 新增分类
 */
async function addBaseClassify (title,type) {
  return await db.collection('tablist').add({
      data:{
        title,
        type
      }
    })
 }

/**
 * 删除分类
 */
async function deleteClassify (id) {
  return await db.collection('tablist').doc(id).remove()
 }



module.exports = {
  adddata,
  getdata,
  msgSecCheck,
  imgSecCheck,
  search,
  getswiper,
  like,
  gettab,
  getdetails,
  getnotice,
  addPostComment,
  addPostChildComment,
  getPostComments,
  deletePostCollectionOrZan,
  addPostZan,
  addPostCollection,
  getPostRelated,
  getReportQrCodeUrl,
  addPostQrCode,
  querySubscribeCount,
  addSubscribeCount,
  getCommentsList,
  changeCommentFlagById,
  setnotice,
  getalldata,
  updatepost,
  addBaseClassify,
  deleteClassify
}