//app.js
// import { promisifyAll, promisify } from 'miniprogram-api-promise';
const utils = require('./utils/utils.js')

App({
  onLaunch: async function () {
    let that = this
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        // env 参数说明：
        //   env 参数决定接下来小程序发起的云开发调用（wx.cloud.xxx）会默认请求到哪个云环境的资源
        //   此处请填入环境 ID, 环境 ID 可打开云控制台查看
        //   如不填则使用默认环境（第一个创建的环境）
        env: 'test-rwbr1',
        traceUser: true,
      })
      let opinid =  wx.getStorageSync('openid')
     if (opinid){
      that.globalData.openid = opinid
     }else{
      let res = await wx.cloud.callFunction({
        name: 'login'
      })
      that.globalData.openid = res.result.openid
      console.log('openid',this.globalData.openid)
      wx.setStorage({
        data: res.result.openid,
        key: 'openid',
      })
     }

    }
  },
  checkUserInfo: function (cb) {
    let that = this
    if (that.globalData.userInfo) {
      typeof cb == "function" && cb(that.globalData.userInfo, true);
    } else {
      wx.getSetting({
        success: function (res) {
          if (res.authSetting['scope.userInfo']) {
            // 已经授权，可以直接调用 getUserInfo 获取头像昵称
            wx.getUserInfo({
              success: function (res) {
                that.globalData.userInfo = JSON.parse(res.rawData);
                typeof cb == "function" && cb(that.globalData.userInfo, true);
              }
            })
          } else {
            typeof cb == "function" && cb(that.globalData.userInfo, false);
          }
        }
      })
    }
  },
  globalData :{
    openid:'',
    userinfo:''
  },
  onPageNotFound(res) {
    wx.switchTab({
      url: 'pages/index/index'
    }) // 如果是 tabbar 页面，请使用 wx.switchTab
  }
})
