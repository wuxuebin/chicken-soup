// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({ env: process.env.Env })

const db = cloud.database()

// 云函数入口函数
exports.main = async (event, context) => {

  let pages = [
    { "path": "pages/index/index", "query": "cid=-1" },
    { "path": "pages/user/user", "query": "cid=-1" }
  ]
  /**
   * 获取详情页id
   * 
   */

  await db.collection('ChickenSoup')
  .field({
    _id: true
  })
  .get().then((res)=>{
    res.data.forEach((item)=>{
      item =`id=${item._id}`
      pages.push({
        "path": "pages/details/details", "query":item
      })
    })
  })
  console.log(pages)
  /*
   * 
   * 页面收录
   * @param {*} event
   * 
   */
  try {
    const result = await cloud.openapi.search.submitPages(
      {
        "pages":pages
      }
    )
    return result
  } catch (err) {
    return err
  }


}