const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  console.log('APPID',wxContext.APPID)
  try {
    const result = await cloud.openapi.subscribeMessage.send({
      touser: wxContext.OPENID,
      page: 'pages/index/index',
      data: {
        thing1: {
          value: '心灵鸡汤'
        },
        thing2: {
          value: '标题'
        },
        date3: {
          value: '2015年01月05日'
        },
        thing4: {
          value: '备注'
        }
      },
      templateId: 'ut_06xUStgr56WOCcPr-7wsIoiKYNLoS2gyXVUoi8LY'
    })
    return result
  } catch (err) {
    return err
  }
}